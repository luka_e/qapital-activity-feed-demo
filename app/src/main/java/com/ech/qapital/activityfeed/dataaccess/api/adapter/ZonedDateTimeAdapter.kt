package com.ech.qapital.activityfeed.dataaccess.api.adapter

import com.squareup.moshi.FromJson

import com.squareup.moshi.ToJson
import java.time.ZoneOffset
import java.time.ZonedDateTime

class ZonedDateTimeAdapter {

    @ToJson
    fun toJson(zonedDateTime: ZonedDateTime) = zonedDateTime.toString()

    @FromJson
    fun fromJson(timestamp: String) =
        ZonedDateTime.parse(timestamp).withZoneSameInstant(ZoneOffset.UTC)

}
