package com.ech.qapital.activityfeed.dataaccess.api

import com.ech.qapital.activityfeed.dataaccess.api.model.ActivitiesResponse
import com.ech.qapital.activityfeed.dataaccess.api.model.UserResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.time.ZonedDateTime

interface ActivitiesApi {

    @GET("activities")
    fun getActivities(
        @Query("from") from: ZonedDateTime,
        @Query("to") to: ZonedDateTime
    ): Single<ActivitiesResponse>

    @GET("users/{id}")
    fun getUser(@Path("id") id: Long): Single<UserResponse>
}
