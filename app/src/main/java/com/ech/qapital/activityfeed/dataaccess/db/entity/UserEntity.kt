package com.ech.qapital.activityfeed.dataaccess.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "user",
    indices = [Index(value = ["display_name"] ,unique = true)])
data class UserEntity(

    @PrimaryKey
    val id: Long?,

    @ColumnInfo(name = "display_name")
    val displayName: String,

    @ColumnInfo(name = "avatar_url")
    val avatarUrl: String
)
