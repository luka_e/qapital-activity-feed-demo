package com.ech.qapital.activityfeed.dataaccess.db.dao

import androidx.room.*
import com.ech.qapital.activityfeed.dataaccess.db.entity.SettingsEntry
import io.reactivex.Single

@Dao
interface SettingsEntryDao {

    @Query("SELECT * FROM settings_entry")
    fun findAll(): Single<List<SettingsEntry>>

    @Query("SELECT * FROM settings_entry WHERE key=:key LIMIT 1")
    fun findByKey(key: String): SettingsEntry?

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(settingsEntry: SettingsEntry): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(settingsEntry: SettingsEntry): Int

    @Query(value = "UPDATE settings_entry SET value=:value WHERE key=:key")
    fun update(key: String, value: String): Int
}