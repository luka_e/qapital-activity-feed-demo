package com.ech.qapital.activityfeed.dataaccess.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ech.qapital.activityfeed.dataaccess.db.entity.UserEntity
import io.reactivex.Maybe

@Dao
interface UserDao {

    @Query("SELECT * FROM user WHERE id=:id")
    fun findById(id: Long): Maybe<UserEntity>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(userEntity: UserEntity): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(users: List<UserEntity>)

}