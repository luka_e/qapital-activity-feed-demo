package com.ech.qapital.activityfeed.ui.feed.list

import androidx.recyclerview.widget.DiffUtil
import com.ech.qapital.activityfeed.dataaccess.FeedActivity

class FeedActivityDiffCallback: DiffUtil.ItemCallback<FeedActivity>() {

    override fun areItemsTheSame(oldItem: FeedActivity, newItem: FeedActivity) = oldItem == newItem

    override fun areContentsTheSame(oldItem: FeedActivity, newItem: FeedActivity) = oldItem == newItem

    override fun getChangePayload(oldItem: FeedActivity, newItem: FeedActivity): Any? {
        if(oldItem.amount.compareTo(newItem.amount) != 0) {
            return newItem.amount
        }

        if(oldItem.message != newItem.message) {
            return newItem.message
        }

        if(oldItem.timestamp.compareTo(newItem.timestamp) != 0) {
            return newItem.timestamp
        }

        if(oldItem.user != newItem.user) {
            return newItem.user
        }
        return null;
    }
}