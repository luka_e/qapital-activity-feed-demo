package com.ech.qapital.activityfeed.ui.feed

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.ech.qapital.activityfeed.ui.feed.list.ActivityFeedAdapter
import com.ech.qapital.activityfeed.ui.feed.list.FeedActivityDiffCallback
import com.ech.qapital.activityfeed.ui.feed.list.ViewHolderFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.qualifiers.ActivityContext

@Module
@InstallIn(FragmentComponent::class)
class FeedModule {

    @Provides
    fun provideActivityFeedAdapter(
        viewHolderFactory: ViewHolderFactory,
        feedActivityDiffCallback: FeedActivityDiffCallback
    ) = ActivityFeedAdapter(viewHolderFactory, feedActivityDiffCallback)

    @Provides
    fun provideViewHolderFactory(requestManager: RequestManager) = ViewHolderFactory(requestManager)

    @Provides
    fun provideRequestManager(@ActivityContext context: Context) = Glide.with(context)

    @Provides
    fun provideFeedActivityDiffCallback() = FeedActivityDiffCallback()
}