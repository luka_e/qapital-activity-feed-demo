package com.ech.qapital.activityfeed.ui.util

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class LoadMoreListener(
    private val threshold: Int,
    private val callback: (() -> Unit)
): RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        val lastVisible = layoutManager.findLastVisibleItemPosition()
        val itemCount = layoutManager.itemCount

        if(dy > 0 && threshold >= itemCount - lastVisible) {
            callback.invoke()
        }
    }
}
