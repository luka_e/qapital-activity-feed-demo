package com.ech.qapital.activityfeed.dataaccess.repository

import com.ech.qapital.activityfeed.dataaccess.db.dao.SettingsEntryDao
import com.ech.qapital.activityfeed.dataaccess.db.entity.SettingsEntry
import io.reactivex.Single
import java.time.Instant
import java.time.ZonedDateTime
import javax.inject.Inject

class SettingsRepository @Inject constructor(
    private val settingsEntryDao: SettingsEntryDao
) {
    enum class SettingsKey {
        KEY_OLDEST_POST
    }

    fun saveOldestActivityTimestamp(oldestActivityTimestamp: Instant) {
        val oldestPostMilist = oldestActivityTimestamp.toEpochMilli().toString()
        if (settingsEntryDao.findByKey(SettingsKey.KEY_OLDEST_POST.name) != null) {
            settingsEntryDao.update(
                SettingsKey.KEY_OLDEST_POST.name,
                oldestPostMilist
            )
        } else {
            settingsEntryDao.insert(
                SettingsEntry(null, SettingsKey.KEY_OLDEST_POST.name, oldestPostMilist)
            )
        }
    }

    fun getOldestActivityTimestamp() =
        Single.just(Instant.MIN).map {
            settingsEntryDao.findByKey(SettingsKey.KEY_OLDEST_POST.name)?.let {
                Instant.ofEpochMilli(it.value.toLong())
            } ?: it
        }
}
