package com.ech.qapital.activityfeed.dataaccess.db.converters

import androidx.room.TypeConverter
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.Instant

class Converters {

    @TypeConverter
    fun bigDecimalToString(bigDecimal: BigDecimal) =
        bigDecimal.setScale(2, RoundingMode.HALF_DOWN).toString()

    @TypeConverter
    fun stringToBigDecimal(number: String) = BigDecimal(number)

    @TypeConverter
    fun instantToLong(instant: Instant) = instant.toEpochMilli()

    @TypeConverter
    fun longToInstant(epochMilli: Long) = Instant.ofEpochMilli(epochMilli)
}