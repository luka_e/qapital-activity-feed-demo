package com.ech.qapital.activityfeed.dataaccess.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "settings_entry",
    indices = [Index(value = ["key"] ,unique = true)]
)
data class SettingsEntry(

    @PrimaryKey
    val id: Long?,

    @ColumnInfo(name = "key")
    val key: String,

    val value: String
)
