package com.ech.qapital.activityfeed.dataaccess.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ech.qapital.activityfeed.dataaccess.db.converters.Converters
import com.ech.qapital.activityfeed.dataaccess.db.dao.ActivityDao
import com.ech.qapital.activityfeed.dataaccess.db.dao.RangeDao
import com.ech.qapital.activityfeed.dataaccess.db.dao.SettingsEntryDao
import com.ech.qapital.activityfeed.dataaccess.db.dao.UserDao
import com.ech.qapital.activityfeed.dataaccess.db.entity.*

@Database(entities = [UserEntity::class, ActivityEntity::class, SettingsEntry::class, RangeEntity::class], version = 1)
@TypeConverters(Converters::class)
abstract class ActivitiesDatbase: RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun activityDao(): ActivityDao
    abstract fun settingsEntryDao(): SettingsEntryDao
    abstract fun rangeDao(): RangeDao
}
