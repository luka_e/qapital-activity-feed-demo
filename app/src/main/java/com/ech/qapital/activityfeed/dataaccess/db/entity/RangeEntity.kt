package com.ech.qapital.activityfeed.dataaccess.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.time.Instant

@Entity(
    tableName = "range",
    indices = [Index(value = ["timeFrom", "timeTo"], unique = true)]
)
data class RangeEntity(

    @PrimaryKey(autoGenerate = true)
    val id: Long?,

    @ColumnInfo(name = "timeFrom")
    val from: Instant,

    @ColumnInfo(name = "timeTo")
    val to: Instant
)
