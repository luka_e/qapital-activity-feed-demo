package com.ech.qapital.activityfeed.service

import com.ech.qapital.activityfeed.dataaccess.db.dao.SettingsEntryDao
import com.ech.qapital.activityfeed.dataaccess.repository.ActivityRepository
import com.ech.qapital.activityfeed.dataaccess.repository.SettingsRepository
import com.ech.qapital.activityfeed.dataaccess.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import java.util.concurrent.Executors

@Module
@InstallIn(ViewModelComponent::class)
class ServiceModule {

    @Provides
    fun provideActivityFeedService(
        activityRepository: ActivityRepository,
        userRepository: UserRepository,
        settingsRepository: SettingsRepository
    ) = ActivityFeedService(activityRepository, userRepository, settingsRepository)

    @Provides
    fun provideExecutorService() = Executors.newFixedThreadPool(4)

}