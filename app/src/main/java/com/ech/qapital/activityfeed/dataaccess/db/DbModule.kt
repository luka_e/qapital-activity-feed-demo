package com.ech.qapital.activityfeed.dataaccess.db

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(ViewModelComponent::class)
class DbModule {

    @Provides
    fun provideDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, ActivitiesDatbase::class.java, "activities-db").build()

    @Provides
    fun provideActivityDao(activitiesDatbase: ActivitiesDatbase) = activitiesDatbase.activityDao()

    @Provides
    fun provideRangeDao(activitiesDatbase: ActivitiesDatbase) = activitiesDatbase.rangeDao()

    @Provides
    fun provideSettingsDao(activitiesDatbase: ActivitiesDatbase) = activitiesDatbase.settingsEntryDao()

    @Provides
    fun provideUserDao(activitiesDatbase: ActivitiesDatbase) = activitiesDatbase.userDao()
}
