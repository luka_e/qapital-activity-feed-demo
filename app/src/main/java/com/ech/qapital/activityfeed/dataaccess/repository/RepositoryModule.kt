package com.ech.qapital.activityfeed.dataaccess.repository

import com.ech.qapital.activityfeed.dataaccess.api.ActivitiesApi
import com.ech.qapital.activityfeed.dataaccess.db.dao.ActivityDao
import com.ech.qapital.activityfeed.dataaccess.db.dao.RangeDao
import com.ech.qapital.activityfeed.dataaccess.db.dao.SettingsEntryDao
import com.ech.qapital.activityfeed.dataaccess.db.dao.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import java.util.concurrent.ExecutorService

@Module
@InstallIn(ViewModelComponent::class)
class RepositoryModule {

    @Provides
    fun provideUserRepository(
        activitiesApi: ActivitiesApi,
        userDao: UserDao,
        executorService: ExecutorService
    ) = UserRepository(activitiesApi, userDao, executorService)

    @Provides
    fun provideActvityRepository(
        activitiesApi: ActivitiesApi,
        activityDao: ActivityDao,
        rangeDao: RangeDao,
        settingsRepository: SettingsRepository,
        executorService: ExecutorService
    ) = ActivityRepository(activitiesApi, activityDao, rangeDao, settingsRepository, executorService)

    @Provides
    fun provideSettingsRepository(settingsEntryDao: SettingsEntryDao) =
        SettingsRepository(settingsEntryDao)
}
