package com.ech.qapital.activityfeed.dataaccess.db.entity

import androidx.room.Embedded
import androidx.room.Relation

data class ActivityWithUser(

    @Embedded
    val activity: ActivityEntity,

    @Relation(
        parentColumn = "user_id",
        entityColumn = "id"
    )
    val userEntity: UserEntity
)
