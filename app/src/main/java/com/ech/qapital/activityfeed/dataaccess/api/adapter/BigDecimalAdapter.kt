package com.ech.qapital.activityfeed.dataaccess.api.adapter

import com.squareup.moshi.FromJson

import com.squareup.moshi.ToJson
import java.math.BigDecimal

class BigDecimalAdapter {

    @ToJson
    fun toJson(bigDecimal: BigDecimal): String {
        return bigDecimal.toString()
    }

    @FromJson
    fun fromJson(number: String): BigDecimal {
        return BigDecimal(number)
    }
}