package com.ech.qapital.activityfeed.dataaccess

import java.math.BigDecimal
import java.security.MessageDigest
import java.time.Instant
import java.util.*

data class User(
    val userId: Long,
    val displayName: String,
    val avatarUrl: String
)

data class Activity(
    val message: String,
    val amount: BigDecimal,
    val userId: Long,
    val timestamp: Instant
)

data class FeedActivity(
    val message: String,
    val amount: BigDecimal,
    val user: User,
    val timestamp: Instant
)

data class SettingsEntry (
    val key: String,
    val value: String
)


fun Activity.calculateHash(): String {
    val digest = MessageDigest.getInstance("SHA-1")
    digest.update(this.toString().encodeToByteArray())
    return Base64.getEncoder().encodeToString(digest.digest())
}
