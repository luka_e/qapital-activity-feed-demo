package com.ech.qapital.activityfeed.dataaccess.api.adapter

import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type
import java.time.ZonedDateTime

class ZonedDateTimeConverterFactory: Converter.Factory() {

    override fun stringConverter(
        type: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<*, String>? {

        return if(type == ZonedDateTime::class.java) {
            Converter<ZonedDateTime, String> { it.toString() }
        } else {
            super.stringConverter(type, annotations, retrofit)
        }
    }
}