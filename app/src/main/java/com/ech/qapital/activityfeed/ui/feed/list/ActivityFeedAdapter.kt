package com.ech.qapital.activityfeed.ui.feed.list

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ech.qapital.activityfeed.dataaccess.FeedActivity
import com.ech.qapital.activityfeed.dataaccess.User
import java.math.BigDecimal
import java.time.Instant
import javax.inject.Inject

class ActivityFeedAdapter @Inject constructor(
    private val viewHolderFactory: ViewHolderFactory,
    feedActivityDiffCallback: FeedActivityDiffCallback
) : ListAdapter<FeedActivity, RecyclerView.ViewHolder>(feedActivityDiffCallback) {

    var isLoading: Boolean = false

    private lateinit var recyclerView: RecyclerView

    fun addActivities(activities: List<FeedActivity>) {
        isLoading = false
        val previous = if(currentList.isNotEmpty()) currentList.subList(0, currentList.size - 1) else listOf()
        submitList( previous + activities + listOf(dummyActivity)) {
            if(!isLoading) {
                recyclerView.postDelayed({
                    toggleLoadingAnimation(false)
                }, 500)
            }
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    fun clearFeed() = submitList(emptyList())

    fun toggleLoadingAnimation(enable: Boolean) {
        isLoading = enable
        recyclerView.post {
            notifyItemChanged(itemCount - 1)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        viewHolderFactory.createViewHolder(parent, viewType)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            FeedActivityViewHolder.TYPE ->
                (holder as FeedActivityViewHolder).bind(getItem(position))
            LoadingItemViewHolder.TYPE ->
                (holder as LoadingItemViewHolder).bind(isLoading)
        }
    }

    override fun getItemCount() = currentList.size

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1 && getItem(position) == dummyActivity) {
            LoadingItemViewHolder.TYPE
        } else {
            FeedActivityViewHolder.TYPE
        }
    }

    fun getLastItem(): FeedActivity = currentList[currentList.size - 2]

    private val dummyActivity = FeedActivity("dummy", BigDecimal.ZERO, User(0, "dummy", "dummy"), Instant.MIN)
}
