package com.ech.qapital.activityfeed.dataaccess.db.dao

import androidx.room.*
import com.ech.qapital.activityfeed.dataaccess.db.entity.RangeEntity
import io.reactivex.Single
import java.time.Instant

@Dao
interface RangeDao {

    @Query("SELECT * FROM range")
    fun loadAllRanges(): Single<List<RangeEntity>>

    @Query("SELECT * FROM range WHERE timeFrom<=:paramFrom and timeTo>=:paramFrom or timeFrom<=:paramTo and timeTo>=:paramTo ORDER BY timeFrom ASC")
    fun findRangesByTwoPoints(paramFrom: Instant, paramTo: Instant): Single<List<RangeEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(rangeEntity: RangeEntity): Long

    @Delete
    fun delete(ranges: RangeEntity)
}