package com.ech.qapital.activityfeed.ui.feed

import androidx.lifecycle.ViewModel
import com.ech.qapital.activityfeed.R
import com.ech.qapital.activityfeed.service.ActivityFeedService
import com.ech.qapital.activityfeed.dataaccess.repository.SettingsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.time.Instant
import javax.inject.Inject

@HiltViewModel
class ActivityFeedPresenter @Inject constructor(
    private val activityFeedService: ActivityFeedService,
    private val settingsRepository: SettingsRepository,
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    private lateinit var view: ActivityFeedView

    fun onViewCreated(view: ActivityFeedView) {
        this.view = view
        fetchData(Instant.now())
        view.toggleMainLoadingAnimation(true)
    }

    fun onRefresh() {
        view.clearFeed()
        view.toggleInlineLoadingAnimation(false)
        if (view.isLoading()) {
            compositeDisposable.clear()
        }
        fetchData(Instant.now())
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }

    fun getLoadMoreTreshold() = 5

    private fun fetchData(lastItemTimestamp: Instant) {
        lastItemTimestamp.minusMillis(1)
        compositeDisposable.add(activityFeedService.fetchFeed(lastItemTimestamp.minusMillis(1))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnError {
                view.showError(R.string.err_failed_to_fetch_feed)
                view.toggleMainLoadingAnimation(false)
            }.subscribe { feed ->
                if(view.itemCount() == 0 && feed.isEmpty()) {
                    view.toggleEmptyText(true)
                    view.toggleList(false)
                } else {
                    view.toggleEmptyText(false)
                    view.toggleList(true)
                    view.updateFeed(feed)
                }
                view.toggleMainLoadingAnimation(false)
            }
        )
    }

    fun loadMore() {
        if (view.isLoading()) {
            return
        }
        view.toggleInlineLoadingAnimation(true)
        val lastTimestamp = view.getLastItem().timestamp
        compositeDisposable.add(settingsRepository.getOldestActivityTimestamp()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { oldest ->
                if (lastTimestamp.isAfter(oldest)) {
                    fetchData(lastTimestamp)
                } else {
                    view.toggleInlineLoadingAnimation(false)
                }
            }
        )
    }
}
