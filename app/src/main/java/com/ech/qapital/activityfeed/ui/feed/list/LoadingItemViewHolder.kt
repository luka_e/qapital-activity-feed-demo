package com.ech.qapital.activityfeed.ui.feed.list

import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.ech.qapital.activityfeed.databinding.ListItemLoadingBinding

class LoadingItemViewHolder(
    private val binding: ListItemLoadingBinding
): RecyclerView.ViewHolder(binding.root) {

    fun bind(loading: Boolean) {
        binding.progressInline.isVisible = loading
    }

    companion object {
        const val TYPE = 1
    }
}
