package com.ech.qapital.activityfeed.dataaccess.api.model

import java.math.BigDecimal
import java.time.ZonedDateTime

data class ActivitiesResponse(
    val oldest: ZonedDateTime,
    val activities: List<ResponseActivity>
)

data class ResponseActivity(
    val message: String,
    val amount: BigDecimal,
    val userId: Long,
    val timestamp: ZonedDateTime
)

data class UserResponse(
    val userId: Long,
    val displayName: String,
    val avatarUrl: String
)