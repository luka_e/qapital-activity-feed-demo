package com.ech.qapital.activityfeed.dataaccess.api

import com.ech.qapital.activityfeed.dataaccess.api.adapter.BigDecimalAdapter
import com.ech.qapital.activityfeed.dataaccess.api.adapter.ZonedDateTimeAdapter
import com.ech.qapital.activityfeed.dataaccess.api.adapter.ZonedDateTimeConverterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
@InstallIn(ViewModelComponent::class)
class ApiModule {

    @Provides
    fun provideActivitiesApi(
        moshi: Moshi,
        okHttpClient: OkHttpClient
    ): ActivitiesApi {
        return Retrofit.Builder()
            .baseUrl("http://qapital-ios-testtask.herokuapp.com")
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addConverterFactory(ZonedDateTimeConverterFactory())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
            .create(ActivitiesApi::class.java)
    }

    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .add(ZonedDateTimeAdapter())
            .add(BigDecimalAdapter())
            .addLast(KotlinJsonAdapterFactory())
            .build()
    }

    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    }
}