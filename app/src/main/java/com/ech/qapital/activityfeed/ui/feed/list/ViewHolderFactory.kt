package com.ech.qapital.activityfeed.ui.feed.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.ech.qapital.activityfeed.databinding.ListItemActivityFeedBinding
import com.ech.qapital.activityfeed.databinding.ListItemLoadingBinding
import javax.inject.Inject

class ViewHolderFactory @Inject constructor(
    private val glideRequestManager: RequestManager
) {

    fun createViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when(viewType) {
            FeedActivityViewHolder.TYPE -> {
                val binding = ListItemActivityFeedBinding.inflate(
                    inflater, parent, false
                )
                FeedActivityViewHolder(binding, glideRequestManager)
            }
            else -> {
                val binding = ListItemLoadingBinding.inflate(
                    inflater, parent, false
                )
                LoadingItemViewHolder(binding)
            }
        }
    }
}
