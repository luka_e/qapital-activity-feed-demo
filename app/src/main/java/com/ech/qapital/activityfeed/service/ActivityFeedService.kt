package com.ech.qapital.activityfeed.service

import com.ech.qapital.activityfeed.dataaccess.Activity
import com.ech.qapital.activityfeed.dataaccess.FeedActivity
import com.ech.qapital.activityfeed.dataaccess.User
import com.ech.qapital.activityfeed.dataaccess.repository.ActivityRepository
import com.ech.qapital.activityfeed.dataaccess.repository.SettingsRepository
import com.ech.qapital.activityfeed.dataaccess.repository.UserRepository
import io.reactivex.Single
import java.time.Instant
import java.time.temporal.ChronoUnit
import javax.inject.Inject

class ActivityFeedService @Inject constructor(
    private val activityRepository: ActivityRepository,
    private val userRepository: UserRepository,
    private val settingsRepository: SettingsRepository
) {

    fun fetchFeed(to: Instant): Single<List<FeedActivity>> =
        fetchActivities(to)
            .flatMapObservable { activities ->
                val userIds = activities.map { it.userId }.toSet()

                val userObservable = userRepository.fetchUsers(userIds).toObservable()

                userObservable.flatMapIterable { users ->
                    activities.map { activity ->
                        mapUserToActivity(activity, users.first { it.userId == activity.userId })
                    }
                }
            }.toSortedList { a1, a2 ->
                a2.timestamp.compareTo(a1.timestamp)
            }

    private fun fetchActivities(to: Instant): Single<List<Activity>> {
        val from = to.minus(14, ChronoUnit.DAYS)
        return activityRepository.fetchActivities(from, to).flatMap {
            if (it.isEmpty() && canContinue(from, to)) {
                fetchActivities(from)
            } else {
                Single.just(it)
            }
        }
    }

    private fun canContinue(from: Instant, to: Instant) =
        from != to && from.isBefore(settingsRepository.getOldestActivityTimestamp().blockingGet())


    private fun mapUserToActivity(activity: Activity, user: User) =
        FeedActivity(
            activity.message,
            activity.amount,
            user,
            activity.timestamp
        )
}
