package com.ech.qapital.activityfeed.dataaccess.repository

import android.util.Log
import com.ech.qapital.activityfeed.dataaccess.User
import com.ech.qapital.activityfeed.dataaccess.api.ActivitiesApi
import com.ech.qapital.activityfeed.dataaccess.db.dao.UserDao
import com.ech.qapital.activityfeed.dataaccess.db.entity.UserEntity
import io.reactivex.Observable
import io.reactivex.Single
import java.lang.Exception
import java.util.concurrent.ExecutorService

class UserRepository(
    private val activitiesApi: ActivitiesApi,
    private val userDao: UserDao,
    private val executorService: ExecutorService
) {

    fun fetchUsers(userIds: Set<Long>): Single<List<User>> {
        return Observable.fromIterable(userIds).flatMapSingle {
            loadUserFromCache(it).switchIfEmpty(Single.defer {
                loadFromApi(it)
            })
        }.toList()
    }

    private fun loadUserFromCache(userId: Long) =
        userDao.findById(userId).map {
            User(
                it.id!!,
                it.displayName,
                it.avatarUrl
            )
        }

    private fun loadFromApi(userId: Long) =
        activitiesApi.getUser(userId).map {
            User(
                it.userId,
                it.displayName,
                it.avatarUrl
            )
        }.map {
            storeUser(it)
            it
        }

    private fun storeUser(user: User) {
        executorService.execute {
            try {
                userDao.insert(UserEntity(
                    user.userId,
                    user.displayName,
                    user.avatarUrl
                ))
            } catch (e: Exception) {
                //TODO logger
                Log.e(UserRepository::class.qualifiedName, "failed to store user")
            }
        }
    }
}
