package com.ech.qapital.activityfeed.ui.feed.list

import android.content.Context
import android.text.Html
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.ech.qapital.activityfeed.R
import com.ech.qapital.activityfeed.databinding.ListItemActivityFeedBinding
import com.ech.qapital.activityfeed.dataaccess.FeedActivity
import java.math.RoundingMode
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

class FeedActivityViewHolder(
    private val binding: ListItemActivityFeedBinding,
    private val glideRequestManager: RequestManager
): RecyclerView.ViewHolder(binding.root) {

    private lateinit var feedActivity: FeedActivity

    fun bind(feedActivity: FeedActivity) {

        this.feedActivity = feedActivity

        glideRequestManager
            .load(feedActivity.user.avatarUrl)
            .circleCrop()
            .placeholder(R.drawable.ic_avatar_placeholder)
            .into(binding.ivAvatar)

        val context = binding.tvAmount.context
        val amountString = feedActivity
            .amount
            .setScale(2, RoundingMode.HALF_DOWN)
            .toString()
        binding.tvAmount.text = context.getString(R.string.format_currency, amountString)

        binding.tVMessage.text = Html.fromHtml(feedActivity.message, Html.FROM_HTML_MODE_COMPACT)

        binding.tvDate.text = resolveDate(feedActivity.timestamp, context)
    }

    private fun resolveDate(timestamp: Instant, context: Context): String {
        val now = ZonedDateTime.now()
        val zonedTimestamp = ZonedDateTime.ofInstant(timestamp, now.zone)
        return when(ChronoUnit.DAYS.between(now, zonedTimestamp)) {
            0L -> context.getString(R.string.date_today)
            1L -> context.getString(R.string.date_yesterday)
            else -> zonedTimestamp.withZoneSameInstant(ZoneId.of(now.zone.id)).format(
                DateTimeFormatter.ofPattern(context.getString(R.string.date_format_feed)))
        }
    }

    companion object {
        const val TYPE = 0
    }
}
