package com.ech.qapital.activityfeed.ui.feed

import com.ech.qapital.activityfeed.dataaccess.FeedActivity

interface ActivityFeedView {

    fun showError(textId: Int)

    fun updateFeed(feed: List<FeedActivity>)

    fun toggleInlineLoadingAnimation(enable: Boolean)

    fun clearFeed()

    fun toggleMainLoadingAnimation(enable: Boolean)

    fun isLoading(): Boolean

    fun getLastItem(): FeedActivity

    fun toggleList(visible: Boolean)

    fun toggleEmptyText(visible: Boolean)

    fun itemCount(): Int
}