package com.ech.qapital.activityfeed.dataaccess.db.dao

import androidx.room.*
import com.ech.qapital.activityfeed.dataaccess.db.entity.ActivityEntity
import com.ech.qapital.activityfeed.dataaccess.db.entity.ActivityWithUser
import io.reactivex.Single
import java.time.Instant

@Dao
interface ActivityDao {

    /*
    @Transaction
    @Query("SELECT * FROM activity WHERE timestamp>=:from and timestamp<=:to")
    fun findBetweenDatesInclusive(
        from: Instant,
        to: Instant
    ): Single<List<ActivityWithUser>>
     */

    @Query("SELECT * FROM activity WHERE timestamp>=:from and timestamp<=:to")
    fun findBetweenDatesInclusive(
        from: Instant,
        to: Instant
    ): Single<List<ActivityEntity>>

    @Query("SELECT MIN(timestamp) from activity")
    fun getOldestTimestamp(): Instant

    @Query("SELECT MAX(timestamp) from activity")
    fun getNewestTimestamp(): Instant

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(activity: ActivityEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(activities: List<ActivityEntity>)
}
