package com.ech.qapital.activityfeed.ui.feed

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.ech.qapital.activityfeed.databinding.FragmentActivityFeedBinding
import com.ech.qapital.activityfeed.dataaccess.FeedActivity
import com.ech.qapital.activityfeed.ui.feed.list.ActivityFeedAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import androidx.recyclerview.widget.DividerItemDecoration
import com.ech.qapital.activityfeed.ui.util.LoadMoreListener

@AndroidEntryPoint
class ActivityFeedFragment : ActivityFeedView, Fragment() {

    private val presenter: ActivityFeedPresenter by viewModels()
    private lateinit var binding: FragmentActivityFeedBinding
    @Inject lateinit var adapter: ActivityFeedAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.let {
        binding = FragmentActivityFeedBinding.inflate(it)
        binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(this)
        binding.swipeRefreshLayout.setOnRefreshListener(presenter::onRefresh)
        val dividerItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        binding.activityList.addItemDecoration(dividerItemDecoration)
        binding.activityList.overScrollMode = View.OVER_SCROLL_NEVER
        binding.activityList.adapter = adapter
        binding.activityList.addOnScrollListener(LoadMoreListener(presenter.getLoadMoreTreshold()) {
            presenter.loadMore()
        })
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun showError(textId: Int) =
        Toast.makeText(context, textId, Toast.LENGTH_LONG).show()

    override fun updateFeed(feed: List<FeedActivity>) = adapter.addActivities(feed)

    override fun toggleInlineLoadingAnimation(enable: Boolean) = adapter.toggleLoadingAnimation(enable)

    override fun clearFeed() = adapter.clearFeed()

    override fun toggleMainLoadingAnimation(enable: Boolean) {
        binding.swipeRefreshLayout.post { binding.swipeRefreshLayout.isRefreshing = enable }
    }

    override fun isLoading() = binding.swipeRefreshLayout.isRefreshing || adapter.isLoading

    override fun getLastItem() = adapter.getLastItem()

    override fun toggleEmptyText(visible: Boolean) {
        binding.tvNoItems.isVisible = visible
    }

    override fun toggleList(visible: Boolean) {
        binding.activityList.isVisible = visible
    }

    override fun itemCount() = adapter.itemCount - 1
}
