package com.ech.qapital.activityfeed.dataaccess.db.entity

import androidx.room.*
import java.math.BigDecimal
import java.time.Instant

@Entity(tableName = "activity")
data class ActivityEntity(

    @PrimaryKey
    val hash: String,

    val message: String,

    val amount: BigDecimal,

    @ColumnInfo(name = "user_id")
    val userId: Long,

    @ColumnInfo(index = true)
    val timestamp: Instant
)
