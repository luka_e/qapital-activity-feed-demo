package com.ech.qapital.activityfeed.dataaccess.repository

import android.util.Log
import com.ech.qapital.activityfeed.dataaccess.Activity
import com.ech.qapital.activityfeed.dataaccess.api.ActivitiesApi
import com.ech.qapital.activityfeed.dataaccess.calculateHash
import com.ech.qapital.activityfeed.dataaccess.db.dao.ActivityDao
import com.ech.qapital.activityfeed.dataaccess.db.dao.RangeDao
import com.ech.qapital.activityfeed.dataaccess.db.entity.ActivityEntity
import com.ech.qapital.activityfeed.dataaccess.db.entity.RangeEntity
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.time.Instant
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.concurrent.ExecutorService

class ActivityRepository(
    private val activitiesApi: ActivitiesApi,
    private val activityDao: ActivityDao,
    private val rangeDao: RangeDao,
    private val settingsRepository: SettingsRepository,
    private val executorService: ExecutorService
) {

    fun fetchActivities(from: Instant, to: Instant): Single<List<Activity>> {
        // TODO complex intersections
        return hasEntireRange(from, to).flatMap {
            if (it) {
                loadUserFromCache(from, to)
            } else {
                loadFromApi(from, to)
            }
        }
    }

    private fun loadUserFromCache(from: Instant, to: Instant) =
        activityDao.findBetweenDatesInclusive(from, to).map {
            it.map { entity ->
                Activity(
                    entity.message,
                    entity.amount,
                    entity.userId,
                    entity.timestamp
                )
            }
        }

    private fun loadFromApi(from: Instant, to: Instant) =
        activitiesApi.getActivities(
            ZonedDateTime.ofInstant(from, ZoneOffset.UTC),
            ZonedDateTime.ofInstant(to, ZoneOffset.UTC)
        ).map {
            settingsRepository.saveOldestActivityTimestamp(it.oldest.toInstant())
            it.activities.map { responseActivity ->
                Activity(
                    responseActivity.message,
                    responseActivity.amount,
                    responseActivity.userId,
                    responseActivity.timestamp.toInstant()
                )
            }.also { activities ->
                storeActivities(from, to, activities)
            }
        }

    private fun storeActivities(from: Instant, to: Instant, activities: List<Activity>) {
        executorService.execute {
            try {
                val dbActivities = activities.map {
                    ActivityEntity(
                        it.calculateHash(),
                        it.message,
                        it.amount,
                        it.userId,
                        it.timestamp
                    )
                }
                activityDao.insertAll(dbActivities)
                storeRange(from, to)
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e(ActivityRepository::class.qualifiedName, "error storing activities")
            }
        }
    }

    private fun storeRange(from: Instant, to: Instant) {
        rangeDao.findRangesByTwoPoints(from, to)
            .map { ranges ->
                val points = mutableListOf<Instant>()
                ranges.forEach {
                    points.add(it.from)
                    points.add(it.to)
                    rangeDao.delete(it)
                }
                points.add(from)
                points.add(to)
                val min = points.minByOrNull { it.toEpochMilli() }
                val max = points.maxByOrNull { it.toEpochMilli() }
                val range = RangeEntity(null, min!!, max!!)
                rangeDao.insert(range)
            }
            .subscribe()
            .dispose()
    }

    private fun hasEntireRange(from: Instant, to: Instant) =
        rangeDao.findRangesByTwoPoints(from, to).map { intersections ->
            intersections.size == 1 && intersections[0].from < from && intersections[0].to > to
        }
}
