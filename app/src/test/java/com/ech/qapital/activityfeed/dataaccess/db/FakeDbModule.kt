package com.ech.qapital.activityfeed.dataaccess.db

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [DbModule::class])
class FakeDbModule {

    @Provides
    fun provideActivitiesDatbase(@ApplicationContext context: Context) =
        Room.inMemoryDatabaseBuilder(context, ActivitiesDatbase::class.java).build()

    @Provides
    fun provideActivityDao(activitiesDatbase: ActivitiesDatbase) = activitiesDatbase.activityDao()

    @Provides
    fun provideRangeDao(activitiesDatbase: ActivitiesDatbase) = activitiesDatbase.rangeDao()

    @Provides
    fun provideSettingsDao(activitiesDatbase: ActivitiesDatbase) = activitiesDatbase.settingsEntryDao()

    @Provides
    fun provideUserDao(activitiesDatbase: ActivitiesDatbase) = activitiesDatbase.userDao()
}
