package com.ech.qapital.activityfeed.dataaccess.repository

import com.ech.qapital.activityfeed.dataaccess.db.dao.SettingsEntryDao
import com.ech.qapital.activityfeed.dataaccess.db.entity.SettingsEntry
import io.mockk.*
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.time.Instant

@RunWith(JUnit4::class)
class SettingsRepositoryTest {

    @MockK
    private lateinit var settingsEntryDao: SettingsEntryDao

    @InjectMockKs
    private lateinit var settingsRepository: SettingsRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        settingsRepository = SettingsRepository(settingsEntryDao)
    }

    @Test
    fun `correctly stores the oldest activity timestamp`() {
        val settingsEntrySlot = slot<SettingsEntry>()
        every { settingsEntryDao.insert(capture(settingsEntrySlot)) } answers { 1 }
        every { settingsEntryDao.findByKey(any()) } returns null

        val timestamp = Instant.now()
        settingsRepository.saveOldestActivityTimestamp(timestamp)

        val settingsEntry = settingsEntrySlot.captured

        assert(settingsEntry.key == SettingsRepository.SettingsKey.KEY_OLDEST_POST.name)
        val instant = Instant.ofEpochMilli(settingsEntry.value.toLong())

        assert(instant == Instant.ofEpochMilli(timestamp.toEpochMilli()))
    }

    @Test
    fun `updates the entry if it already exists`() {
        val settingsEntryMock = mockk<SettingsEntry>()
        val key = SettingsRepository.SettingsKey.KEY_OLDEST_POST.name
        every { settingsEntryDao.findByKey(key) } returns settingsEntryMock

        val keySlot = slot<String>()
        val valueSlot = slot<String>()
        every { settingsEntryDao.update(capture(keySlot), capture(valueSlot)) } answers { 1 }

        val timestamo = Instant.now()
        settingsRepository.saveOldestActivityTimestamp(timestamo)

        assert(keySlot.captured == key)
        assert(valueSlot.captured == timestamo.toEpochMilli().toString())
    }

    @Test
    fun `provides values provided by the dao`() {
        val timestamp = Instant.ofEpochMilli(Instant.now().toEpochMilli())
        val settingsEntryMock = mockk<SettingsEntry>()
        every { settingsEntryMock.value } returns timestamp.toEpochMilli().toString()
        val key = SettingsRepository.SettingsKey.KEY_OLDEST_POST.name
        every { settingsEntryDao.findByKey(key) } returns settingsEntryMock

        val result = settingsRepository.getOldestActivityTimestamp().blockingGet()

        assert(result == timestamp)
    }

    @Test
    fun `returns Instant MIN if the value doesn't exist`() {
        val key = SettingsRepository.SettingsKey.KEY_OLDEST_POST.name
        every { settingsEntryDao.findByKey(key) } returns null
        val result = settingsRepository.getOldestActivityTimestamp().blockingGet()
        assert(result == Instant.MIN)
    }
}
