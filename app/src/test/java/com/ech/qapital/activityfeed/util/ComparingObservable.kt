package com.ech.qapital.activityfeed.util

import assertk.assertThat
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.isTrue
import io.reactivex.Observer
import io.reactivex.disposables.Disposable

class ComparingObserver<T>: Observer<T> {

    val data = mutableListOf<T>()

    private var throwable: Throwable? = null

    private var isComplete = false

    private var disposable: Disposable? = null

    override fun onSubscribe(d: Disposable) {
        disposable = d
    }

    override fun onNext(t: T) {
        data.add(t)
    }

    override fun onError(e: Throwable) {
        throwable = e
    }

    override fun onComplete() {
        isComplete = true
    }

    fun assertComplete() {
        assertThat(isComplete).isTrue()
        checkData()
        assertThat(data.size).isEqualTo(2)
    }

    fun assertDataEquivalent() {
        assertThat(data.size).isEqualTo(2)
        checkData()
        if(data.first() is Collection<*>) {
            assertThat((data.first() as Collection<*>)).containsExactlyInAnyOrder(data.last())
        } else {
            assertThat(data.first()).isEqualTo(data.last())
        }
    }

    fun assertNoErrorThrown() = throwable?.let { throw it }

    fun assertErrorThrown() = assertThat(throwable).isNotNull()

    private fun checkData() {
        if(data.size == 1 && data[0] is Collection<*> && (data[0] as Collection<*>).size == 2) {
            val subCollection = (data[0] as Collection<*>)
            data.clear()
            data.addAll(subCollection.map { it as T })
        }
    }
}