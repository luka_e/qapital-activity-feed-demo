package com.ech.qapital.activityfeed.dataaccess.db.dao

import com.ech.qapital.activityfeed.dataaccess.db.BaseDbTest
import com.ech.qapital.activityfeed.dataaccess.db.entity.UserEntity
import com.ech.qapital.activityfeed.util.ComparingObserver
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@HiltAndroidTest
@Config(application = HiltTestApplication::class)
@RunWith(RobolectricTestRunner::class)
class UserEntityDaoTest : BaseDbTest() {

    @Before
    override fun setUp() {
        super.setUp()
    }

    @Test
    fun `can create user and fetch it from db`() {
        val userDao = activitiesDatabase.userDao()
        val initialData = Single.just(
            UserEntity(null, "displayName1", "avatarUrl1")
        ).map {
            val id = userDao.insert(it)
            it.copy(id = id)
        }

        val resultObserver = ComparingObserver<UserEntity>()
        val resultObservable = userDao.findById(1).toSingle()

        Single.concat(initialData, resultObservable)
            .toObservable()
            .subscribeOn(Schedulers.io())
            .blockingSubscribe(resultObserver)

        resultObserver.assertNoErrorThrown()
        resultObserver.assertComplete()
        resultObserver.assertDataEquivalent()
    }

    @Test
    fun `cannot insert existing diyplay name`() {
        val userDao = activitiesDatabase.userDao()
        val resultObserver = ComparingObserver<Set<Long>>()
        Single.just(
            UserEntity(null, "displayName1", "avatarUrl1")
        ).map {
            val id = userDao.insert(it)
            val secondId = userDao.insert(it)
            linkedSetOf(id, secondId)
        }.toObservable()
            .subscribeOn(Schedulers.io())
            .blockingSubscribe(resultObserver)

        resultObserver.assertErrorThrown()
    }
}
