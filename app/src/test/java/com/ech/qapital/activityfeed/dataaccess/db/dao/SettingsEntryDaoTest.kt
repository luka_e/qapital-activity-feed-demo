package com.ech.qapital.activityfeed.dataaccess.db.dao

import com.ech.qapital.activityfeed.dataaccess.db.BaseDbTest
import com.ech.qapital.activityfeed.dataaccess.db.entity.SettingsEntry
import com.ech.qapital.activityfeed.util.ComparingObserver
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@HiltAndroidTest
@Config(application = HiltTestApplication::class)
@RunWith(RobolectricTestRunner::class)
class SettingsEntryDaoTest : BaseDbTest() {

    @Before
    override fun setUp() {
        super.setUp()
    }

    @Test
    fun `can create new entries in the database and fetch them`() {
        val settingsEntryDao = activitiesDatabase.settingsEntryDao()
        val initialData = Observable.just(
            SettingsEntry(null, "key1", "value1"),
            SettingsEntry(null, "key2", "value2"),
            SettingsEntry(null, "key3", "value3")
        ).map {
            val id = settingsEntryDao.insert(it)
            it.copy(id = id)
        }.toList()

        val resultObserver = TestObserver<List<SettingsEntry>>()
        Single.concat(initialData, settingsEntryDao.findAll())
            .toObservable()
            .subscribeOn(Schedulers.io())
            .blockingSubscribe(resultObserver)

        resultObserver.assertNoErrors()
        resultObserver.assertComplete()
        val data = resultObserver.values()[0]
        resultObserver.assertValueAt(1, data)
    }

    @Test
    fun `can find entries by their key`() {
        val settingsEntryDao = activitiesDatabase.settingsEntryDao()
        val resultObserver = ComparingObserver<SettingsEntry>()
        val key = "key"
        Single.just(SettingsEntry(null, key, "value"))
            .map {
                val id = settingsEntryDao.insert(it)
                it.copy(id = id)
            }
            .concatWith(Single.just(settingsEntryDao).map { it.findByKey(key) })
            .toObservable()
            .subscribeOn(Schedulers.io())
            .blockingSubscribe(resultObserver)

        resultObserver.assertNoErrorThrown()
        resultObserver.assertComplete()
        resultObserver.assertDataEquivalent()
    }

    @Test
    fun `cannot insert existing setting key`() {
        val settingsEntryDao = activitiesDatabase.settingsEntryDao()
        val resultObserver = ComparingObserver<Set<Long>>()
        Single.just(
            SettingsEntry(null, "key", "value")
        ).map {
            val id = settingsEntryDao.insert(it)
            val secondId = settingsEntryDao.insert(it)
            linkedSetOf(id, secondId)
        }.toObservable()
            .subscribeOn(Schedulers.io())
            .blockingSubscribe(resultObserver)

        resultObserver.assertErrorThrown()
    }

    @Test
    fun `can update settings`() {
        val resultObserver = ComparingObserver<List<String>>()
        val key = "someKey"
        val otherValue = "someOtherValue"
        Single.just(
            SettingsEntry(null, key, "inititalValue")
        ).map {
            val settingsEntryDao = activitiesDatabase.settingsEntryDao()
            val id = settingsEntryDao.insert(it)
            val copy = it.copy(id = id, key = key, value = otherValue)
            settingsEntryDao.update(copy)
            val dbResult = settingsEntryDao.findByKey(key)
            listOf(otherValue, dbResult?.value!!)
        }.toObservable()
            .subscribeOn(Schedulers.io())
            .blockingSubscribe(resultObserver)

        resultObserver.assertNoErrorThrown()
        resultObserver.assertComplete()
        resultObserver.assertDataEquivalent()
    }

    @Test
    fun `can update settings by key`() {
        val resultObserver = ComparingObserver<List<String>>()
        val key = "someKey"
        val otherValue = "someOtherValue"
        Single.just(
            SettingsEntry(null, key, "inititalValue")
        ).map {
            val settingsEntryDao = activitiesDatabase.settingsEntryDao()
            settingsEntryDao.insert(it)
            settingsEntryDao.update(key, otherValue)
            val dbResult = settingsEntryDao.findByKey(key)
            listOf(otherValue, dbResult?.value!!)
        }.toObservable()
            .subscribeOn(Schedulers.io())
            .blockingSubscribe(resultObserver)

        resultObserver.assertNoErrorThrown()
        resultObserver.assertComplete()
        resultObserver.assertDataEquivalent()
    }
}