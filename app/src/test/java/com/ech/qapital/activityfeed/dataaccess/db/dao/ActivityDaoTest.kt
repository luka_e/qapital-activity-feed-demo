package com.ech.qapital.activityfeed.dataaccess.db.dao

import assertk.fail
import com.ech.qapital.activityfeed.dataaccess.db.BaseDbTest
import com.ech.qapital.activityfeed.dataaccess.db.entity.ActivityEntity
import com.ech.qapital.activityfeed.dataaccess.db.entity.ActivityWithUser
import com.ech.qapital.activityfeed.dataaccess.db.entity.UserEntity
import com.ech.qapital.activityfeed.util.ComparingObserver
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.Instant
import kotlin.random.Random

@HiltAndroidTest
@Config(application = HiltTestApplication::class)
@RunWith(RobolectricTestRunner::class)
class ActivityDaoTest : BaseDbTest() {

    @Before
    override fun setUp() {
        super.setUp()
    }

    @Test
    fun `finds activities between the date range`() {
        val testActivities = createTestActivities()
        val ranges = createRanges(testActivities)

        val resultComparators = Observable.fromIterable(ranges)
            .map {
                val testFilteredData = testActivities.filter { activity ->
                    activity.timestamp >= it.first &&
                            activity.timestamp <= it.second
                }
                val resultObserver = ComparingObserver<List<ActivityEntity>>()
                Single.merge(
                    Single.just(testFilteredData),
                    activitiesDatabase.activityDao().findBetweenDatesInclusive(it.first, it.second)
                ).toObservable()
                    .observeOn(Schedulers.io())
                    .blockingSubscribe(resultObserver)
                resultObserver
            }.toList()
            .subscribeOn(Schedulers.io())
            .blockingGet()

        resultComparators.forEach {
            it.assertNoErrorThrown()
            it.assertComplete()
            val first = it.data[0]
            val second = it.data[1]

            val notInSecond = first.filter { !second.contains(it) }
            val notInFirst = second.filter { !first.contains(it) }

            if (notInFirst.isNotEmpty()) {
                fail("expected lists to be the same", emptyArray<ActivityWithUser>(), notInFirst)
            }

            if (notInSecond.isNotEmpty()) {
                fail("expected lists to be the same", emptyArray<ActivityWithUser>(), notInSecond)
            }
        }
    }

    private fun createRanges(testActivities: List<ActivityEntity>): List<Pair<Instant, Instant>> {
        val timestamps = testActivities.map { it.timestamp }

        val max = timestamps.last().toEpochMilli()
        val ranges = mutableListOf<Pair<Instant, Instant>>()
        for (i in 0..10) {
            val from = Random.nextLong(0, timestamps.last().toEpochMilli() * 5)
            val to = Random.nextLong(from, max * 10)
            ranges.add(Pair(Instant.ofEpochMilli(from), Instant.ofEpochMilli(to)))
        }
        return ranges
    }

    private fun createTestUsers(): List<UserEntity> {
        val testUsers = mutableListOf<UserEntity>()
        for (i in 0 until 15) {
            testUsers.add(
                UserEntity(null, "name$i", "avatar˘$i")
            )
        }
        return Observable.fromIterable(testUsers)
            .map {
                val id = activitiesDatabase.userDao().insert(it)
                it.copy(id)
            }.toList()
            .subscribeOn(Schedulers.io())
            .blockingGet()
    }

    private fun createTestActivities(): List<ActivityEntity> {
        val users = createTestUsers()
        val testActivities = mutableListOf<ActivityEntity>()
        for (i in 0 until 99) {
            val user = users[Random.nextInt(0, users.size)]
            testActivities.add(
                ActivityEntity(
                    "randomHash$i",
                    "message$i",
                    BigDecimal.valueOf(Random.nextDouble(10.0, 200.0))
                        .setScale(2, RoundingMode.HALF_DOWN),
                    user.id!!,
                    Instant.ofEpochMilli(1000L * 1000 * i)
                )
            )
        }
        Observable.fromIterable(testActivities)
            .map {
                activitiesDatabase.activityDao().insert(it)
            }.toList()
            .subscribeOn(Schedulers.io())
            .blockingGet()

        return testActivities
    }
}
