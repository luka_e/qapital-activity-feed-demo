package com.ech.qapital.activityfeed.dataaccess.repository

import com.ech.qapital.activityfeed.dataaccess.api.ActivitiesApi
import com.ech.qapital.activityfeed.dataaccess.api.model.UserResponse
import com.ech.qapital.activityfeed.dataaccess.db.dao.UserDao
import com.ech.qapital.activityfeed.dataaccess.db.entity.UserEntity
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Maybe
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.util.concurrent.ExecutorService

class UserRepositoryTest {

    @MockK
    private lateinit var activitiesApi: ActivitiesApi

    @MockK
    private lateinit var userDao: UserDao

    @MockK
    private lateinit var executorService: ExecutorService

    private lateinit var userRepository: UserRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        userRepository = UserRepository(
            activitiesApi,
            userDao,
            executorService
        )
    }

    @Test
    fun `loads user from db if exists`() {

        val mockUserEntity = mockk<UserEntity>()
        every { mockUserEntity.avatarUrl } returns "mockUrl"
        every { mockUserEntity.displayName } returns "mockName"
        every { mockUserEntity.id } returns 1L
        every { userDao.findById(eq(1L)) } returns Maybe.just(mockUserEntity)

        val result = userRepository.fetchUsers(setOf(1L)).blockingGet()

        verify { userDao.findById(eq(1L)) }
        verify { activitiesApi wasNot Called }

        assert(result.size == 1)
        assert(result.first().avatarUrl == mockUserEntity.avatarUrl)
        assert(result.first().displayName == mockUserEntity.displayName)
        assert(result.first().userId == mockUserEntity.id)
    }

    @Test
    fun `loads user from api if not in db`() {

        every { userDao.findById(eq(1L)) } returns Maybe.empty()

        val mockUserResponse = mockk<UserResponse>()
        every { mockUserResponse.avatarUrl } returns "mockUrl"
        every { mockUserResponse.displayName } returns "mockName"
        every { mockUserResponse.userId } returns 1L
        every { activitiesApi.getUser(eq(1L)) } returns Single.just(mockUserResponse)

        val result = userRepository.fetchUsers(setOf(1L)).blockingGet()

        verify { userDao.findById(eq(1L)) }
        verify { activitiesApi.getUser(eq(1L)) }

        assert(result.size == 1)
        assert(result.first().avatarUrl == mockUserResponse.avatarUrl)
        assert(result.first().displayName == mockUserResponse.displayName)
        assert(result.first().userId == mockUserResponse.userId)
    }

    @Test
    fun `saves user in db when received from api`() {

        every { userDao.findById(eq(1L)) } returns Maybe.empty()

        val mockUserResponse = mockk<UserResponse>()
        every { mockUserResponse.avatarUrl } returns "mockUrl"
        every { mockUserResponse.displayName } returns "mockName"
        every { mockUserResponse.userId } returns 1L
        every { activitiesApi.getUser(eq(1L)) } returns Single.just(mockUserResponse)
        every { userDao.insert(any()) } returns 1L

        userRepository.fetchUsers(setOf(1L)).blockingGet()

        verify { userDao.findById(eq(1L)) }
        verify { activitiesApi.getUser(eq(1L)) }

        val runnableSlot = slot<Runnable>()
        verify { executorService.execute(capture(runnableSlot)) }
        runnableSlot.captured.run()

        val userEntitySlot = slot<UserEntity>()
        verify { userDao.insert(capture(userEntitySlot)) }
        val result = userEntitySlot.captured

        assert(result.avatarUrl == mockUserResponse.avatarUrl)
        assert(result.displayName == mockUserResponse.displayName)
        assert(result.id == mockUserResponse.userId)
    }
}