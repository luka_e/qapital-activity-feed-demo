package com.ech.qapital.activityfeed.service

import com.ech.qapital.activityfeed.dataaccess.Activity
import com.ech.qapital.activityfeed.dataaccess.User
import com.ech.qapital.activityfeed.dataaccess.repository.ActivityRepository
import com.ech.qapital.activityfeed.dataaccess.repository.SettingsRepository
import com.ech.qapital.activityfeed.dataaccess.repository.UserRepository
import io.mockk.*
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.math.BigDecimal
import java.time.Instant
import java.time.temporal.ChronoUnit
import kotlin.random.Random

@RunWith(JUnit4::class)
class ActivityFeedServiceTest {

    @MockK
    private lateinit var activityRepository: ActivityRepository

    @MockK
    private lateinit var userRepository: UserRepository

    @MockK
    private lateinit var settingsRepository: SettingsRepository

    @InjectMockKs
    private lateinit var activityFeedService: ActivityFeedService

    private val oldest = Instant.now().minus(60, ChronoUnit.DAYS)

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        activityFeedService =
            ActivityFeedService(activityRepository, userRepository, settingsRepository)
        every { settingsRepository.getOldestActivityTimestamp() } returns Single.just(oldest)
        setupUserMock()
    }

    @Test
    fun `should correctly call the APIs`() {
        val to = Instant.now()
        val from = to.minus(14, ChronoUnit.DAYS)
        val activities = activitiesMock(from, to, 1)
        every { activityRepository.fetchActivities(any(), any()) } returns Single.just(activities)
        activityFeedService.fetchFeed(to).blockingGet()

        val toSlot = slot<Instant>()
        val fromSlot = slot<Instant>()
        verify(exactly = 1) {
            activityRepository.fetchActivities(from = capture(fromSlot), to = capture(toSlot))
        }
        assert(toSlot.captured == to)
        assert(fromSlot.captured == from)

        val userIdsSlot = slot<Set<Long>>()
        verify(exactly = 1) { userRepository.fetchUsers(capture(userIdsSlot)) }

        assert(activities.map { it.userId }.toList() == userIdsSlot.captured.toList())
    }

    @Test
    fun `should loop fetching activities on empty array until a positive response`() {
        every { settingsRepository.getOldestActivityTimestamp() } returns Single.just(Instant.now().plus(365, ChronoUnit.DAYS))

        var counter = 0

        every { activityRepository.fetchActivities(any(), any()) } answers {
            counter++
            if(counter < 10) {
                Single.just(listOf())
            } else {
                Single.just(activitiesMock(firstArg(), secondArg(), 10))
            }
        }

        activityFeedService.fetchFeed(Instant.now()).blockingGet()
        assert(counter == 10)
    }

    @Test
    fun `should quit looping when it gets to the oldest activity`() {
        every { activityRepository.fetchActivities(any(), any()) } answers{ Single.just(listOf())}

        var error: Throwable? = null
        var postedEmptyList = false
        activityFeedService.fetchFeed(Instant.now())
            .toObservable()
            .doOnError {
                error = it
            }
            .blockingSubscribe {
                postedEmptyList = it.isEmpty()
            }
        assert(error == null)
        assert(postedEmptyList)
    }

    @Test
    fun `should not call the user API too many times`() {
        val to = Instant.now()
        val from = to.minus(14, ChronoUnit.DAYS)

        val activities = activitiesMock(from, to, 10)

        every { activityRepository.fetchActivities(any(), any()) } returns Single.just(activities)

        val uniqueUsers = activities.map { it.userId }.toSet().size

        activityFeedService.fetchFeed(to).blockingGet()

        val userIdsSlot = slot<Set<Long>>()
        verify(exactly = 1) { userRepository.fetchUsers(capture(userIdsSlot)) }
        assert(uniqueUsers == userIdsSlot.captured.size)
    }

    private fun activitiesMock(
        from: Instant,
        to: Instant,
        number: Int
    ): MutableList<Activity> {
        val activities = mutableListOf<Activity>()
        for (i in 0 until number) {
            val activityMock = mockk<Activity>()
            every { activityMock.amount } returns BigDecimal.valueOf(Random(i).nextDouble())
            every { activityMock.message } returns "someMessage $i"
            val millis = Random.nextLong(from.toEpochMilli(), to.toEpochMilli())
            every { activityMock.timestamp } returns Instant.ofEpochMilli(millis)
            val userIdUpperBound = if (number < 2) 2 else number
            every { activityMock.userId } returns Random.nextLong(1, userIdUpperBound.toLong())
            activities.add(activityMock)
        }
        return activities
    }

    private fun setupUserMock() {
        val userIdsSlot = slot<Set<Long>>()
        every { userRepository.fetchUsers(capture(userIdsSlot)) } answers {
            val userList = mutableListOf<User>()
            userIdsSlot.captured.forEach { userId ->
                val mockUser = mockk<User>()
                every { mockUser.avatarUrl } returns "avatarUrl$userId"
                every { mockUser.displayName } returns "displayName$userId"
                every { mockUser.userId } returns userId
                userIdsSlot.clear()
                userList.add(mockUser)
            }
            Single.just(userList)
        }
    }
}
