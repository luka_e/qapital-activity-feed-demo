package com.ech.qapital.activityfeed.dataaccess.repository

import com.ech.qapital.activityfeed.dataaccess.api.ActivitiesApi
import com.ech.qapital.activityfeed.dataaccess.api.model.ActivitiesResponse
import com.ech.qapital.activityfeed.dataaccess.api.model.ResponseActivity
import com.ech.qapital.activityfeed.dataaccess.db.dao.ActivityDao
import com.ech.qapital.activityfeed.dataaccess.db.dao.RangeDao
import com.ech.qapital.activityfeed.dataaccess.db.entity.ActivityEntity
import com.ech.qapital.activityfeed.dataaccess.db.entity.RangeEntity
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.time.Instant
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.concurrent.ExecutorService

class ActivityRepositoryTest {

    @MockK
    private lateinit var activitiesApi: ActivitiesApi

    @MockK
    private lateinit var activityDao: ActivityDao

    @MockK
    private lateinit var rangeDao: RangeDao

    @MockK
    private lateinit var settingsRepository: SettingsRepository

    @MockK
    private lateinit var executorService: ExecutorService

    private lateinit var activityRepository: ActivityRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        activityRepository = ActivityRepository(
            activitiesApi,
            activityDao,
            rangeDao,
            settingsRepository,
            executorService
        )
    }

    @Test
    fun `loads items from db when whole range is in db`() {
        every { rangeDao.findRangesByTwoPoints(any(), any()) } returns
                Single.just(listOf(RangeEntity(1, Instant.MIN, Instant.MAX)))

        val mockActivityEntity = mockk<ActivityEntity>()
        every { mockActivityEntity.amount } returns mockk()
        every { mockActivityEntity.message } returns "mockMessage"
        every { mockActivityEntity.userId } returns 1L
        every { mockActivityEntity.timestamp } returns mockk()
        every { activityDao.findBetweenDatesInclusive(any(), any()) } returns
                Single.just(listOf(mockActivityEntity))

        val from = Instant.ofEpochMilli(0)
        val to = Instant.ofEpochMilli(1)
        val result = activityRepository.fetchActivities(from, to).blockingGet()

        verify { activityDao.findBetweenDatesInclusive(eq(from), eq(to)) }

        assert(result.size == 1)
        assert(result.first().amount == mockActivityEntity.amount)
        assert(result.first().message == mockActivityEntity.message)
        assert(result.first().userId == mockActivityEntity.userId)
        assert(result.first().timestamp == mockActivityEntity.timestamp)
    }

    @Test
    fun `loads items from api when not whole range is in db`() {
        every { rangeDao.findRangesByTwoPoints(any(), any()) } returns Single.just(listOf())

        val mockActivityResponse = mockk<ActivitiesResponse>()
        val mockResponseActivity = mockk<ResponseActivity>()
        setupApiResponse(mockActivityResponse, mockResponseActivity)

        val from = Instant.ofEpochMilli(0)
        val to = Instant.ofEpochMilli(10)

        val result = activityRepository.fetchActivities(from, to).blockingGet()

        val zonedFrom = ZonedDateTime.ofInstant(from, ZoneOffset.UTC)
        val zonedTo = ZonedDateTime.ofInstant(to, ZoneOffset.UTC)

        verify { activitiesApi.getActivities(eq(zonedFrom), eq(zonedTo)) }

        assert(result.size == 1)
        assert(result.first().amount == mockResponseActivity.amount)
        assert(result.first().message == mockResponseActivity.message)
        assert(result.first().userId == mockResponseActivity.userId)
        assert(result.first().timestamp == mockResponseActivity.timestamp.toInstant())
    }

    @Test
    fun `store items to db after loading from api`() {
        every { rangeDao.findRangesByTwoPoints(any(), any()) } returns Single.just(listOf())

        val mockActivityResponse = mockk<ActivitiesResponse>()
        val mockResponseActivity = mockk<ResponseActivity>()
        setupApiResponse(mockActivityResponse, mockResponseActivity)

        val from = Instant.ofEpochMilli(0)
        val to = Instant.ofEpochMilli(10)

        activityRepository.fetchActivities(from, to).blockingGet()

        val zonedFrom = ZonedDateTime.ofInstant(from, ZoneOffset.UTC)
        val zonedTo = ZonedDateTime.ofInstant(to, ZoneOffset.UTC)
        verify { activitiesApi.getActivities(eq(zonedFrom), eq(zonedTo)) }

        val runnableSlot = slot<Runnable>()
        verify { executorService.execute(capture(runnableSlot)) }
        runnableSlot.captured.run()

        val activityEntitiesSlot = slot<List<ActivityEntity>>()
        verify { activityDao.insertAll(capture(activityEntitiesSlot)) }

        val result = activityEntitiesSlot.captured

        assert(result.size == 1)
        assert(result.first().amount == mockResponseActivity.amount)
        assert(result.first().message == mockResponseActivity.message)
        assert(result.first().userId == mockResponseActivity.userId)
        assert(result.first().timestamp == mockResponseActivity.timestamp.toInstant())
    }

    @Test
    fun `stores oldest timestamo after loading from api`() {
        every { rangeDao.findRangesByTwoPoints(any(), any()) } returns Single.just(listOf())

        val mockActivityResponse = mockk<ActivitiesResponse>()
        val mockResponseActivity = mockk<ResponseActivity>()
        setupApiResponse(mockActivityResponse, mockResponseActivity)

        val from = Instant.ofEpochMilli(0)
        val to = Instant.ofEpochMilli(10)

        activityRepository.fetchActivities(from, to).blockingGet()

        verify { settingsRepository.saveOldestActivityTimestamp(eq(Instant.ofEpochMilli(1))) }
    }

    @Test
    fun `stores range after loading from api`() {
        every { rangeDao.findRangesByTwoPoints(any(), any()) } returns Single.just(listOf())

        val mockActivityResponse = mockk<ActivitiesResponse>()
        val mockResponseActivity = mockk<ResponseActivity>()
        setupApiResponse(mockActivityResponse, mockResponseActivity)

        val from = Instant.ofEpochMilli(10)
        val to = Instant.ofEpochMilli(20)

        every { rangeDao.findRangesByTwoPoints(eq(from), eq(to)) } returns Single.just(listOf())

        activityRepository.fetchActivities(from, to).blockingGet()

        val runnableSlot = slot<Runnable>()
        verify { executorService.execute(capture(runnableSlot)) }
        runnableSlot.captured.run()

        val expectedRange = RangeEntity(null, Instant.ofEpochMilli(10), Instant.ofEpochMilli(20))

        verify(exactly = 0) { rangeDao.delete(any()) }
        verify { rangeDao.insert(eq(expectedRange)) }
    }

    @Test
    fun `merges ranges after loading from api`() {
        every { rangeDao.findRangesByTwoPoints(any(), any()) } returns Single.just(listOf())

        val mockActivityResponse = mockk<ActivitiesResponse>()
        val mockResponseActivity = mockk<ResponseActivity>()
        setupApiResponse(mockActivityResponse, mockResponseActivity)

        val from = Instant.ofEpochMilli(10)
        val to = Instant.ofEpochMilli(20)

        val existingRanges = listOf(
            RangeEntity(1, Instant.ofEpochMilli(5), Instant.ofEpochMilli(11)),
            RangeEntity(2, Instant.ofEpochMilli(18), Instant.ofEpochMilli(23))
        )

        every { rangeDao.findRangesByTwoPoints(eq(from), eq(to)) } returns Single.just(
            existingRanges
        )

        activityRepository.fetchActivities(from, to).blockingGet()

        val runnableSlot = slot<Runnable>()
        verify { executorService.execute(capture(runnableSlot)) }
        runnableSlot.captured.run()

        val expectedRange = RangeEntity(null, Instant.ofEpochMilli(5), Instant.ofEpochMilli(23))

        verify { rangeDao.delete(existingRanges.first()) }
        verify { rangeDao.delete(existingRanges.last()) }
        verify { rangeDao.insert(eq(expectedRange)) }
    }

    private fun setupApiResponse(
        mockActivityResponse: ActivitiesResponse,
        mockResponseActivity: ResponseActivity
    ) {
        every { mockResponseActivity.amount } returns mockk()
        every { mockResponseActivity.message } returns "mockMessage"
        every { mockResponseActivity.userId } returns 1L
        val mockTimestamp = mockk<ZonedDateTime>()
        every { mockResponseActivity.timestamp } returns mockTimestamp
        every { mockTimestamp.toInstant() } returns Instant.ofEpochMilli(5)
        every { mockActivityResponse.activities } returns listOf(mockResponseActivity)
        val mockOldest = mockk<ZonedDateTime>()
        every { mockActivityResponse.oldest } returns mockOldest
        every { mockOldest.toInstant() } returns Instant.ofEpochMilli(1)
        every {
            activitiesApi.getActivities(
                any(),
                any()
            )
        } returns Single.just(mockActivityResponse)
    }
}
