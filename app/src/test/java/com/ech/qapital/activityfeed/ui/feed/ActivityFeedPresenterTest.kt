package com.ech.qapital.activityfeed.ui.feed

import com.ech.qapital.activityfeed.R
import com.ech.qapital.activityfeed.dataaccess.FeedActivity
import com.ech.qapital.activityfeed.dataaccess.repository.SettingsRepository
import com.ech.qapital.activityfeed.service.ActivityFeedService
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.time.Instant
import java.util.concurrent.TimeUnit


@RunWith(JUnit4::class)
class ActivityFeedPresenterTest {

    @MockK
    private lateinit var activityFeedService: ActivityFeedService

    @MockK
    private lateinit var settingsRepository: SettingsRepository

    @MockK
    private lateinit var view: ActivityFeedView

    @MockK
    private lateinit var mockDataSingle: Single<List<FeedActivity>>

    @MockK
    private lateinit var mockOldestSingle: Single<Instant>

    @MockK
    private lateinit var mockDisposable: Disposable

    private val errorConsumerSlot = slot<Consumer<Throwable>>()
    private val dataConsumerSlot = slot<Consumer<List<FeedActivity>>>()
    private val oldestConsumerSlot = slot<Consumer<Instant>>()

    private val fakeScheduler = object : Scheduler() {
        override fun createWorker() = ExecutorScheduler.ExecutorWorker(Runnable::run, false)
        override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit) =
            super.scheduleDirect(run, 0, unit)
    }


    private lateinit var activityFeedPresenter: ActivityFeedPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        activityFeedPresenter = ActivityFeedPresenter(activityFeedService, settingsRepository)
        setupMocks()
        RxJavaPlugins.setInitIoSchedulerHandler { fakeScheduler }
        RxJavaPlugins.setInitComputationSchedulerHandler { fakeScheduler }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { fakeScheduler }
        RxJavaPlugins.setInitSingleSchedulerHandler { fakeScheduler }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { fakeScheduler }
    }

    private fun setupMocks() {
        every { activityFeedService.fetchFeed(any()) } returns mockDataSingle
        every { mockDataSingle.subscribeOn(any()) } returns mockDataSingle
        every { mockDataSingle.observeOn(any()) } returns mockDataSingle
        every { mockDataSingle.doOnError(capture(errorConsumerSlot)) } returns mockDataSingle
        every { mockDataSingle.doOnSuccess(any()) } returns mockDataSingle
        every { mockDataSingle.subscribe(capture(dataConsumerSlot)) } returns mockDisposable
        every { mockOldestSingle.subscribeOn(any()) } returns mockOldestSingle
        every { mockOldestSingle.observeOn(any()) } returns mockOldestSingle
        every { mockOldestSingle.subscribe(capture(oldestConsumerSlot)) } returns mockDisposable
        every { settingsRepository.getOldestActivityTimestamp() } returns mockOldestSingle
    }

    @Test
    fun `starts loading data when onViewCreated is triggered`() {
        activityFeedPresenter.onViewCreated(view)
        verify { activityFeedService.fetchFeed(any()) }
    }

    @Test
    fun `starts the loading animation when onViewCreated is triggered`() {
        activityFeedPresenter.onViewCreated(view)
        verify { view.toggleMainLoadingAnimation(eq(true)) }
    }

    @Test
    fun `updates list and ends animation on successful response`() {
        activityFeedPresenter.onViewCreated(view)
        every { view.itemCount() } returns 0
        val mockData = mockk<List<FeedActivity>>()
        every { mockData.isEmpty() } returns false
        dataConsumerSlot.captured.accept(mockData)
        verify { view.toggleList(true) }
        verify { view.toggleEmptyText(false) }
        verify { view.updateFeed(eq(mockData)) }
        verify { view.toggleMainLoadingAnimation(eq(false)) }
    }

    @Test
    fun `shows empty text on empty initial response`() {
        activityFeedPresenter.onViewCreated(view)
        every { view.itemCount() } returns 0
        val mockData = mockk<List<FeedActivity>>()
        every { mockData.isEmpty() } returns true
        dataConsumerSlot.captured.accept(mockData)
        verify { view.toggleList(false) }
        verify { view.toggleEmptyText(true) }
        verify(exactly = 0) { view.updateFeed(eq(mockData)) }
        verify { view.toggleMainLoadingAnimation(eq(false)) }
    }

    @Test
    fun `shows error on view if fetching data throws`() {
        activityFeedPresenter.onViewCreated(view)
        val mockError = mockk<Throwable>()
        errorConsumerSlot.captured.accept(mockError)
        verify { view.showError(eq(R.string.err_failed_to_fetch_feed)) }
        verify { view.toggleMainLoadingAnimation(eq(false)) }
    }

    @Test
    fun `loadMore() fetches data when last item is not oldest`() {
        activityFeedPresenter.onViewCreated(view)
        clearAllMocks()
        setupMocks()
        every { view.isLoading() } returns false
        val mockLastItem = mockk<FeedActivity>()
        every { view.getLastItem() } returns mockLastItem
        val mockLastInstant = mockk<Instant>()
        every { mockLastItem.timestamp } returns mockLastInstant
        every { mockLastInstant.minusMillis(any()) } returns mockLastInstant
        val mockOldestInstant = mockk<Instant>()
        every { mockLastInstant.isAfter(eq(mockOldestInstant)) } returns true
        activityFeedPresenter.loadMore()
        oldestConsumerSlot.captured.accept(mockOldestInstant)
        verifyAll {
            view.isLoading()
            view.getLastItem()
            settingsRepository.getOldestActivityTimestamp()
            activityFeedService.fetchFeed(eq(mockLastInstant))
            view.toggleInlineLoadingAnimation(eq(true))
        }
    }

    @Test
    fun `loadMore() does nothing if already loading`() {
        activityFeedPresenter.onViewCreated(view)
        clearAllMocks()
        every { view.isLoading() } returns true
        activityFeedPresenter.loadMore()
        verifyAll {
            view.isLoading()
            settingsRepository wasNot Called
            activityFeedService wasNot Called
        }
    }

    @Test
    fun `loadMore() doesn't fetch data when last item is oldest`() {
        activityFeedPresenter.onViewCreated(view)
        clearAllMocks()
        setupMocks()
        every { view.isLoading() } returns false
        val mockLastItem = mockk<FeedActivity>()
        every { view.getLastItem() } returns mockLastItem
        val mockLastInstant = mockk<Instant>()
        every { mockLastItem.timestamp } returns mockLastInstant
        every { mockLastInstant.minusMillis(any()) } returns mockLastInstant
        val mockOldestInstant = mockk<Instant>()
        every { mockLastInstant.isAfter(eq(mockOldestInstant)) } returns false
        activityFeedPresenter.loadMore()
        oldestConsumerSlot.captured.accept(mockOldestInstant)
        verifyAll {
            view.isLoading()
            view.toggleInlineLoadingAnimation(eq(true))
            view.getLastItem()
            settingsRepository.getOldestActivityTimestamp()
            activityFeedService wasNot Called
            view.toggleInlineLoadingAnimation(eq(false))
        }
    }

    @Test
    fun `onDestroy clears data disposable`() {
        activityFeedPresenter.onViewCreated(view)
        activityFeedPresenter.onDestroy()
        verify { mockDisposable.dispose() }
    }

    @Test
    fun `onRefresh clears list, list animation and fetches data when not already loading`() {
        activityFeedPresenter.onViewCreated(view)
        clearAllMocks()
        setupMocks()
        every { view.isLoading() } returns false
        activityFeedPresenter.onRefresh()
        verifyAll {
            view.clearFeed()
            view.toggleInlineLoadingAnimation(false)
            view.isLoading()
            activityFeedService.fetchFeed(any())
        }
    }

    @Test
    fun `onRefresh clears list, list animation, disposes existing request and fetches data`() {
        activityFeedPresenter.onViewCreated(view)
        clearAllMocks()
        setupMocks()
        every { view.isLoading() } returns true
        activityFeedPresenter.onRefresh()
        verifyAll {
            view.clearFeed()
            view.toggleInlineLoadingAnimation(false)
            mockDisposable.dispose()
            view.isLoading()
            activityFeedService.fetchFeed(any())
            mockDisposable.hashCode()
        }
    }
}