package com.ech.qapital.activityfeed.dataaccess.db

import dagger.hilt.android.testing.HiltAndroidRule
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import javax.inject.Inject


abstract class BaseDbTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var activitiesDatabase: ActivitiesDatbase

    @Before
    open fun setUp() {
        hiltRule.inject()
        Observable.just(activitiesDatabase)
            .subscribeOn(Schedulers.io())
            .blockingSubscribe()
    }

    @After
    fun tearDown() = activitiesDatabase.close()

}

