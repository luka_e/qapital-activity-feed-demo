package com.ech.qapital.activityfeed.dataaccess.db.dao

import assertk.assertThat
import assertk.assertions.isEmpty
import com.ech.qapital.activityfeed.dataaccess.db.BaseDbTest
import com.ech.qapital.activityfeed.dataaccess.db.entity.RangeEntity
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.time.Instant

@HiltAndroidTest
@Config(application = HiltTestApplication::class)
@RunWith(RobolectricTestRunner::class)
// @LooperMode(LooperMode.Mode.PAUSED)
class RangeEntityDaoTest: BaseDbTest() {

    @Test
    fun `saves data to the DB and fetches it correctly`() {
        val rangeDao = activitiesDatabase.rangeDao()
        val initialData = Observable.just(
            createRange(1, 2),
            createRange(5, 6),
            createRange(100, 200)
        ).map {
            val id = rangeDao.insert(it)
            it.copy(id = id)
        }.toList()

        val resultTestObserver = TestObserver<List<RangeEntity>>()
        Single.concat(initialData, rangeDao.loadAllRanges())
            .toObservable()
            .subscribeOn(Schedulers.io())
            .blockingSubscribe(resultTestObserver)

        resultTestObserver.assertComplete()
        resultTestObserver.assertNoErrors()
        val data = resultTestObserver.values().first()
        resultTestObserver.assertValueAt(1, data)
    }

    @Test
    fun `finds intersecting intervals`() {
        val rangeDao = activitiesDatabase.rangeDao()
        val initialData = Observable.just(
            createRange(4, 22),
            createRange(33, 44),
            createRange(55, 100)
        ).map {
            val id = rangeDao.insert(it)
            it.copy(id = id)
        }.toList()
            .subscribeOn(Schedulers.io())
            .blockingGet()

        val params = listOf(
            1 to 5,
            11 to 22,
            18 to 36,
            66 to 77,
            200 to 300
        )

        val expectedResults = listOf(
            listOf(initialData[0]),
            listOf(initialData[0]),
            listOf(initialData[0], initialData[1]),
            listOf(initialData[1], initialData[2]),
            listOf()
        )

        val instantParams = params.map {
            Instant.ofEpochMilli(it.first.toLong()) to Instant.ofEpochMilli(it.second.toLong())
        }
        val resultPairs = Observable.fromIterable(instantParams)
            .flatMapSingle {
                rangeDao.findRangesByTwoPoints(it.first, it.second)
            }.zipWith(expectedResults) { result, expected ->
                result to expected
            }.toList()
            .subscribeOn(Schedulers.io())
            .blockingGet()

        resultPairs.forEach { pair ->
            assertThat(pair.first.filter { !pair.second.contains(it) }).isEmpty()
        }
    }

    private fun createRange(from: Int, to: Int) =
        RangeEntity(null, Instant.ofEpochMilli(from.toLong()), Instant.ofEpochMilli(to.toLong()))

}